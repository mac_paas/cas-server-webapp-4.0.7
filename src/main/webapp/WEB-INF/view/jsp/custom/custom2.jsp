<!DOCTYPE html>
<%@ page session="true" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>单点登录</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="">
  <link href="res/style.css" rel="stylesheet">
  <link href="res/layui/css/layui.css" rel="stylesheet" >
  <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.staticfile.org/font-awesome/3.2.1/css/font-awesome.min.css">
  <!-- JS -->
  <script src="https://cdn.staticfile.org/jquery/3.4.1/jquery.min.js"></script>
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="https://cdn.staticfile.org/html5shiv/3.7.3/html5shiv.min.js"></script>
  <![endif]-->
  <script src="res/layui/layui.js" charset="utf-8"></script>
  <script src="res/check.js" charset="utf-8"></script>
</head>

<body>

<!-- Form area -->
<div class="admin-form">

    <div class="row">
      <div class="col-md-12">
        <!-- Widget starts -->
        <div class="widget worange">
          <!-- Widget head -->
          <div class="widget-head">
            <i class="icon-lock"></i> 登陆页面2
          </div>

          <div class="widget-content">
            <div class="padd">
              <!-- Login form -->
              <form:form method="post" id="fm1" cssClass="form-horizontal" commandName="${commandName}"  htmlEscape="true">

                <!-- Username -->
                <div class="form-group">
                  <label class="control-label col-lg-3" for="username">账户</label>
                  <div class="col-lg-9">
                    <input type="text" class="form-control" id="username" name="username" value="${username}" placeholder="用户名/邮箱/手机号">
                  </div>
                </div>
                <!-- Password -->
                <div class="form-group">
                  <label class="control-label col-lg-3" for="password">密码</label>
                  <div class="col-lg-9">
                    <input type="password" class="form-control" id="password" name="password" placeholder="请输入密码">
                  </div>
                </div>
                <!-- Password -->
                <%--<div class="form-group">
                  <label class="control-label col-lg-3" for="password">验证码</label>
                  <div class="col-lg-4">
                    <input type="text" class="form-control" id="authcode" name="authcode" placeholder="输入验证码">
                  </div>
                  <div class="col-lg-5">
                    <img class="col-lg-12" onclick="this.src='captcha.jpg?'+Math.random()" src="captcha.jpg">
                  </div>

                </div>--%>

                <div class="form-group">
                  <div class="col-lg-9 col-lg-offset-2">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> 记住密码
                      </label>
                      <label style="color: #d9534f;">${msg}</label>
                    </div>
                  </div>
                </div>
                <input type="hidden" name="isNeedValid" value="true" />
                <input type="hidden" name="lt" value="${loginTicket}" />
                <input type="hidden" name="execution" value="${flowExecutionKey}" />
                <input type="hidden" name="_eventId" value="submit" />
                <div class="col-lg-9 col-lg-offset-2">
                  <button type="submit" onclick="return check(this.form);"  class="btn btn-danger">登陆</button>
                  <button type="reset"  class="btn btn-default" >重置</button>
                </div>
                <br />
              </form:form>

            </div>
          </div>

          <div class="widget-foot">
            没有账号? <a href="#">立即注册</a>
          </div>
        </div>
      </div>
    </div>

</div>
</body>
</html>