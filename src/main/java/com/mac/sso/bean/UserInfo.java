package com.mac.sso.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * UserInfo entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "user_info")
public class UserInfo implements java.io.Serializable {

    private static final long serialVersionUID = -8017178229381500057L;
    private Integer uid;
    private String username;
    private String password;
    private String realname;

    public UserInfo() {
    }

    public UserInfo(Integer uid, String username, String password, String realname) {
        this.uid = uid;
        this.username = username;
        this.password = password;
        this.realname = realname;
    }
    @Id
    @Column(name = "uid", unique = true, nullable = false, length = 50)
    public Integer getUid() {
        return uid;
    }
    public void setUid(Integer uid) {
        this.uid = uid;
    }
    @Column(name = "username", length = 100)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    @Column(name = "password", length = 100)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    @Column(name = "realname", length = 100)
    public String getRealname() {
        return this.realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "uid=" + uid +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", realname='" + realname + '\'' +
                '}';
    }
}
