package com.mac.sso.bean;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.jasig.cas.authentication.UsernamePasswordCredential;
/**
 * @author： byy
 * @date : 2017年12月4日 上午11:54:20
 * @Description：带验证码的登陆页面
 */
public class UsernamePasswordCredentialWithAuthCode extends UsernamePasswordCredential {

    private static final long serialVersionUID = 1L;
    //验证码
    @NotNull
    @Size(min = 1, message = "required.authcode")
    private String authcode;
    public final String getAuthcode() {
        return authcode;
    }
    public final void setAuthcode(String authcode) {
        this.authcode = authcode;
    }
}

