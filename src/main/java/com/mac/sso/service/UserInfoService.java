package com.mac.sso.service;

import com.mac.sso.bean.UserInfo;
import com.mac.sso.dao.UserInfoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserInfoService {

    @Autowired
    private UserInfoDAO userInfoDAO;

    public UserInfo findByUsername(String username){

        return userInfoDAO.findByUsername(username);
    }

}
