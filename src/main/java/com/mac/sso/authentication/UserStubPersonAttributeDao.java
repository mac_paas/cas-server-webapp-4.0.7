package com.mac.sso.authentication;

import com.mac.sso.bean.UserInfo;
import com.mac.sso.service.UserInfoService;
import org.jasig.services.persondir.IPersonAttributes;
import org.jasig.services.persondir.support.AttributeNamedPersonImpl;
import org.jasig.services.persondir.support.StubPersonAttributeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author： byy
 * @date : 2017年12月5日 下午2:55:48
 * @Description：自定义的返回给客户端相关信息
 */
@Component(value = "attributeRepository")
public class UserStubPersonAttributeDao extends StubPersonAttributeDao {

    @Autowired
    private UserInfoService userInfoService;

    public IPersonAttributes getPerson(String uid) {
        Map<String, List<Object>> attributes = new HashMap<String, List<Object>>();
        try {
            UserInfo userInfo=userInfoService.findByUsername(uid);

            attributes.put("realname", Collections.singletonList((Object)URLEncoder.encode(userInfo.getRealname()!=null?userInfo.getRealname():"", "UTF-8")));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new AttributeNamedPersonImpl(attributes);
    }
}
